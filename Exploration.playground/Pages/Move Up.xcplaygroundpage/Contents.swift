//: [Previous](@previous)

import UIKit

/*
 Thinking more about how a learner can manipulate points.
 
 This takes some good ideas from Apple maps, when the learner grabs a hold of a point it moves above the finger.
 
 This demo also scales the point when it's being interacted with so that it feels more like a point and less like a region.
 
 From *super* quick user testing, there was suprise that the point got smaller when it began being interacted with
 */

class PointView: UIView {
    enum State {
        case AtRest
        case InMotion
    }
    
    var state: State = .AtRest {
        didSet {
            switch(self.state){
            case .AtRest:
                self.pointLabel.isHidden = true
            case .InMotion:
                self.pointLabel.isHidden = false
            }
        }
    }
    
    var displayPosition: CGPoint = CGPoint.zero {
        didSet {
            pointLabel.text = "(\(Int(self.displayPosition.x)), \(Int(self.displayPosition.y)))"
            pointLabel.sizeToFit()
        }
    }
    let point: UIView = {
        let point = UIView(frame: CGRect(x: 0, y:0, width: 20, height: 20))
        point.layer.cornerRadius = 10
        point.backgroundColor = .orange()
        return point
    }()
    
    var pointTransform: CGAffineTransform = CGAffineTransform.identity {
        didSet {
            self.point.transform = pointTransform
        }
    }
    
    let pointLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = #colorLiteral(red: 0.600000023841858, green: 0.600000023841858, blue: 0.600000023841858, alpha: 1.0)
        return label
    }()
    
    init(){
        super.init(frame: CGRect(x: 0, y:0, width: 60, height: 60))
        
        point.center = self.center
        
        pointLabel.text = "(\(self.displayPosition.x),\(self.displayPosition.y))"
        pointLabel.sizeToFit()
        addSubview(pointLabel)
        addSubview(point)
        
        ({ self.state = .AtRest })()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
/*
 next steps
 - bring up x and y lines to make clearer where point is
 - don't let the point get out of it's superview
 */

class ViewController: UIViewController {
    let point: PointView = {
        return PointView()
    }()
    
    
    let touch: UIView = {
        let view = UIView(frame: CGRect(x: 0, y:0, width: 40, height: 40))
        view.layer.cornerRadius = 20
        view.layer.borderColor = (#colorLiteral(red: 0.803921580314636, green: 0.803921580314636, blue: 0.803921580314636, alpha: 1.0) as UIColor).cgColor
        view.layer.borderWidth = 2
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white()
        self.point.center = CGPoint(x: 120, y:160)
        self.view.addSubview(point)
        self.view.addSubview(touch)
        self.touch.isHidden = true
        
        let pressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.longPress(recognizer:)))
        pressGesture.minimumPressDuration = 0.1
        point.addGestureRecognizer(pressGesture)
        
    }
    
    func longPress(recognizer: UILongPressGestureRecognizer
        ){
        let offset = UIOffset(horizontal: 0, vertical: -50)
        let location = recognizer.location(in: view)
        let target = CGPoint(x: location.x + offset.horizontal, y: location.y + offset.vertical)
        self.touch.center = location
        self.point.displayPosition = location
        
        switch(recognizer.state){
        case .began:
            self.touch.isHidden = false
            self.point.state = .InMotion
            UIView.animate(withDuration: 0.2){
                self.point.center = target
                self.point.pointTransform = CGAffineTransform(scaleX: 0.3, y: 0.3)
            }
        case .ended, .cancelled:
            self.touch.isHidden = true
            self.point.state = .AtRest
            UIView.animate(withDuration: 0.2){
                self.point.pointTransform = CGAffineTransform(scaleX: 1, y: 1)
            }
        default:
            self.point.state = .InMotion
            self.point.center = target
        }
    }
}

import PlaygroundSupport
let vc = ViewController()
PlaygroundPage.current.liveView = vc

