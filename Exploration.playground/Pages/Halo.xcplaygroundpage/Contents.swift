import UIKit

class InteractivePoint: UIView {
    let pointLocation: UILabel
    let pointLabel: UILabel
    let connectionLine: CAShapeLayer
    let halo: CAShapeLayer
    var offset: CGPoint?
    var label = "A" {
        didSet {
            pointLabel.text = label
        }
    }
    
    override var center: CGPoint {
        didSet {
            super.center = center
            pointLocation.text = "(\(Int(center.x)), \(Int(center.y)))"
            pointLocation.sizeToFit()
        }
    }
    
    var interacting = false {
        didSet {
            pointLocation.isHidden = !interacting
            connectionLine.isHidden = !interacting
            halo.opacity = interacting ? 0.2 : 0.07
        }
    }
    
    override init(frame: CGRect) {
        pointLocation = UILabel()
        connectionLine = CAShapeLayer()
        halo = CAShapeLayer()
        pointLabel = UILabel()
        
        let frameCenter = CGPoint(x: frame.width/2, y: frame.height/2)
        super.init(frame: frame)
        
        let point = CAShapeLayer()
        point.path = UIBezierPath(arcCenter: frameCenter, radius: 3, startAngle: 0, endAngle: 360, clockwise: true).cgPath
        point.fillColor = UIColor.red().cgColor
        
        halo.path = UIBezierPath(arcCenter: frameCenter, radius: 50, startAngle: 0, endAngle: 360, clockwise: true).cgPath
        halo.fillColor = UIColor.orange().cgColor
        halo.opacity = 0.07
        
        let linePath = UIBezierPath()
        linePath.move(to: frameCenter)
        linePath.addLine(to: CGPoint(x: 100, y: 20))
        connectionLine.path = linePath.cgPath
        connectionLine.lineWidth = 1
        connectionLine.strokeColor = UIColor.red().cgColor
        connectionLine.isHidden = true
        
        pointLocation.text = "(\(center.x), \(center.y))"
        pointLocation.textColor = UIColor.gray()
        pointLocation.sizeToFit()
        pointLocation.frame.origin = CGPoint(x: 100, y: 10)
        pointLocation.isHidden = true
        
        pointLabel.text = label
        pointLabel.frame.origin = CGPoint(x: 53, y:65)
        pointLabel.sizeToFit()
        
        layer.addSublayer(connectionLine)
        layer.addSublayer(halo)
        layer.addSublayer(point)
        
        addSubview(pointLocation)
        addSubview(pointLabel)
        
        let touchHandler = UITapGestureRecognizer(target: self, action: "didTap")
        addGestureRecognizer(touchHandler)
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.didPanPoint(recognizer:)))
        addGestureRecognizer(panGesture)
    }
    
    func didTap(){
        self.pointLabel.textColor = UIColor.red()
        self.interacting = true
        UIView.animate(withDuration: 1.2, animations: {
            self.pointLabel.transform = self.pointLabel.transform.scaleBy(x: 1.6, y: 1.6)
        }) { (_) in
            self.interacting = false
            self.pointLabel.textColor = UIColor.black()
            self.pointLabel.transform = CGAffineTransform.identity
        }
    }
    
    func didPanPoint(recognizer: UIPanGestureRecognizer){
        superview?.bringSubview(toFront: self)
        
        // Set interacting state
        switch recognizer.state {
        case .Ended, .Cancelled:
            interacting = false
        default:
            interacting = true
        }
        
        switch recognizer.state {
        case .Began:
            let location = recognizer.location(in: self)
            offset = CGPoint(x: frame.size.width/2 - location.x,
                             y: frame.size.height/2 - location.y)
        case .Changed:
            print("ok")
            let locationInView = recognizer.location(in: superview)
            let naturalNext = CGPoint(x: locationInView.x + offset!.x,
                                      y: locationInView.y + offset!.y)
            
            let position = superview!.convert(frame, to: superview!.superview)
            center = CGPoint(
                x: max(min(naturalNext.x, position.maxX), position.minX),
                y: max(min(naturalNext.y, position.maxY), superview!.frame.minY)
 )
        default:
            offset = nil
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ViewController: UIViewController {
    var offset: CGPoint?
    var graphPaper: UIImageView!
    
    override func viewDidLoad() {
        view.backgroundColor = UIColor.white()
        
        graphPaper = UIImageView(image: (#imageLiteral(resourceName: "graph-paper.png") as UIImage))
        graphPaper.frame.origin = CGPoint(x: 40, y: 200)

        let point1 = InteractivePoint(frame: CGRect(x: 203, y: 283, width: 120, height: 120))
        point1.label = "A"
        
        let point2 = InteractivePoint(frame: CGRect(x: 90, y: 256, width: 120, height: 120))
        point2.label = "B"
        
        
        view.addSubview(graphPaper)
        view.addSubview(point1)
    }
    
}

import PlaygroundSupport
let viewController = ViewController()
PlaygroundPage.current.liveView = viewController
